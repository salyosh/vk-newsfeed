//
//  VKServerManager.h
//  VK News
//
//  Created by Sergey Alyoshin on 29/12/2018.
//  Copyright © 2018 MERA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VKServerManager : NSObject

+ (VKServerManager *)sharedManager;

- (void)getProfileAvatarOnSuccess:(void(^)(NSString *urlOfProfilePhoto))success
                        onFailure:(void(^)(NSError *error, NSInteger statusCode))failure;

- (void)getPostsWithCount:(NSInteger)count
   startFromIdentificator:(NSString *)next_from
                 onSuccess:(void(^)(NSArray *posts, NSString *nextFromParameter))success
                 onFailure:(void(^)(NSError *error, NSInteger statusCode))failure;

@end
