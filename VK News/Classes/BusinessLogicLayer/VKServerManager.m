//
//  VKServerManager.m
//  VK News
//
//  Created by Sergey Alyoshin on 29/12/2018.
//  Copyright © 2018 MERA. All rights reserved.
//

#import "VKServerManager.h"
#import <VKSdk.h>
#import "Post.h"

static CGFloat versionOfApi = 5.92f;
static int numberOfAttempts = 10;

@implementation VKServerManager

+ (VKServerManager *)sharedManager {
    static VKServerManager *manager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[VKServerManager alloc] init];
    });
    
    return manager;
}

- (void)getProfileAvatarOnSuccess:(void (^)(NSString *))success
                        onFailure:(void (^)(NSError *, NSInteger))failure {
    VKRequest *getProfilePhoto = [[VKApi users] get: @{@"fields" : @"photo_50", @"version" : @(versionOfApi)}];
    getProfilePhoto.attempts = numberOfAttempts;
    
    [getProfilePhoto executeWithResultBlock:^(VKResponse *response) {
        if (success) {
            success(response.json[0][@"photo_50"]);
        }
    } errorBlock:^(NSError *error) {
        if (error.code != VK_API_ERROR) {
            [error.vkError.request repeat];
            
        } else {
            NSLog(@"VK error: %@", error);
            
            if (failure) {
                failure(error, error.code);
            }
        }
    }];
}

- (void)getPostsWithCount:(NSInteger)count
   startFromIdentificator:(NSString *)next_from
                 onSuccess:(void(^)(NSArray *posts, NSString *nextFromParameter))success
                 onFailure:(void(^)(NSError *error, NSInteger statusCode))failure {
    NSDictionary *parametersForVkRequest = [NSDictionary dictionaryWithObjectsAndKeys:
                                            next_from,                 @"start_from",
                                            @"friends, groups, pages", @"source_ids",
                                            @"post",                   @"filters",
                                            @(count),                  @"count",
                                            @(versionOfApi),           @"version",
                                            nil];
    VKRequest *getPosts = [VKRequest requestWithMethod:@"newsfeed.get" parameters:parametersForVkRequest];
    getPosts.attempts = numberOfAttempts;
    
    [getPosts executeWithResultBlock:^(VKResponse *response) {
        NSMutableArray *postsArray = [[NSMutableArray alloc] init];
        
        for (int i = 0; i < count; i++) {
            Post *post = [[Post alloc] initWithCurrentResponse:response andIteration:i];
            
            [postsArray addObject:post];
        }
        if (success)
            success(postsArray, response.json[@"next_from"]);
        
    } errorBlock:^(NSError *error) {
        if (error.code != VK_API_ERROR) {
            [error.vkError.request repeat];
            
        } else {
            NSLog(@"VK error: %@", error);
            
            if (failure) {
                failure(error, error.code);
            }
        }
    }];
}

@end
