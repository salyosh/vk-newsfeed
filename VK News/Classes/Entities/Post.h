//
//  Post.h
//  VK News
//
//  Created by Sergey Aleshin on 28/11/2018.
//  Copyright © 2018 MERA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <VKSdk.h>

@interface Post : NSObject

@property (strong, nonatomic) NSString *nameOfSource;
@property (strong, nonatomic) NSURL *urlOfSourcesAvatar;
@property (strong, nonatomic) NSDate *dateOfPost;
@property (strong, nonatomic) NSString *textOfPost;
@property (strong, nonatomic) NSURL *urlOfPostsPhoto;
@property (assign, nonatomic) CGFloat widthPhoto;
@property (assign, nonatomic) CGFloat heightPhoto;
@property (assign, nonatomic) NSInteger numberOfLikes;
@property (assign, nonatomic) NSInteger numberOfComments;
@property (assign, nonatomic) NSInteger numberOfShares;

- (id)initWithCurrentResponse:(VKResponse *)response andIteration:(NSInteger)i;

@end
