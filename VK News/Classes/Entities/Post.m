//
//  Post.m
//  VK News
//
//  Created by Sergey Aleshin on 28/11/2018.
//  Copyright © 2018 MERA. All rights reserved.
//

#import "Post.h"

@implementation Post

- (id)initWithCurrentResponse:(VKResponse *)response andIteration:(NSInteger)i{
    self = [super init];
    
    if (self) {
        NSPredicate *indexPredicate = [NSPredicate predicateWithFormat:@"id = %ld", ([response.json[@"items"][i][@"source_id"] integerValue] > 0) ? ([response.json[@"items"][i][@"source_id"] integerValue]) : (-[response.json[@"items"][i][@"source_id"] integerValue])];
        
        self.nameOfSource = ([response.json[@"items"][i][@"source_id"] integerValue] > 0) ? ([NSString stringWithFormat:@"%@ %@", [response.json[@"profiles"] filteredArrayUsingPredicate:indexPredicate][0][@"first_name"], [response.json[@"profiles"] filteredArrayUsingPredicate:indexPredicate][0][@"last_name"]]) : ([NSString stringWithFormat:@"%@", [response.json[@"groups"] filteredArrayUsingPredicate:indexPredicate][0][@"name"]]);
        self.urlOfSourcesAvatar = [NSURL URLWithString:([response.json[@"items"][i][@"source_id"] integerValue] > 0) ? ([NSString stringWithFormat:@"%@", [response.json[@"profiles"] filteredArrayUsingPredicate:indexPredicate][0][@"photo_50"]]) : ([NSString stringWithFormat:@"%@", [response.json[@"groups"] filteredArrayUsingPredicate:indexPredicate][0][@"photo_50"]])];
        self.dateOfPost = [NSDate dateWithTimeIntervalSince1970:[response.json[@"items"][i][@"date"] integerValue]];
        self.textOfPost = response.json[@"items"][i][@"text"];
        if (response.json[@"items"][i][@"attachments"][0][@"photo"][@"photo_604"]) {
            self.urlOfPostsPhoto = [NSURL URLWithString:response.json[@"items"][i][@"attachments"][0][@"photo"][@"photo_604"]];
            self.widthPhoto = [response.json[@"items"][i][@"attachments"][0][@"photo"][@"width"] doubleValue];
            self.heightPhoto = [response.json[@"items"][i][@"attachments"][0][@"photo"][@"height"] doubleValue];
        } else {
            self.urlOfPostsPhoto = [NSURL URLWithString:@""];
        }
        self.numberOfLikes = [response.json[@"items"][i][@"likes"][@"count"] integerValue];
        self.numberOfComments = [response.json[@"items"][i][@"comments"][@"count"] integerValue];
        self.numberOfShares = [response.json[@"items"][i][@"reposts"][@"count"] integerValue];
    }
    
    return self;
}

@end
