//
//  StartViewController.h
//  VK News
//
//  Created by Sergey Alyoshin on 20/12/2018.
//  Copyright © 2018 MERA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <VKSdk.h>

@interface StartViewController : UIViewController <VKSdkDelegate>

@end
