//
//  StartViewController.m
//  VK News
//
//  Created by Sergey Alyoshin on 20/12/2018.
//  Copyright © 2018 MERA. All rights reserved.
//

#import "StartViewController.h"
#import "ViewController.h"
#import <UIKit/UIKit.h>

static NSString *const TOKEN_KEY = @"my_application_access_token";
static NSString *const NEXT_CONTROLLER_SEGUE_ID = @"showNewsFeed";
static NSArray *SCOPE = nil;

@interface StartViewController () <UIAlertViewDelegate, VKSdkDelegate, VKSdkUIDelegate>

@property (weak, nonatomic) IBOutlet UIButton *enterButton;

@end

@implementation StartViewController

- (void)viewDidLoad {
    [self.navigationController setNavigationBarHidden:YES];
    self.enterButton.layer.cornerRadius = self.enterButton.frame.size.height / 2;
    SCOPE = @[VK_PER_FRIENDS, VK_PER_WALL];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[VKSdk initializeWithAppId:@"6783351"] registerDelegate:self];
    [[VKSdk instance] setUiDelegate:self];
    [VKSdk wakeUpSession:SCOPE completeBlock:^(VKAuthorizationState state, NSError *error) {
        if (state == VKAuthorizationAuthorized) {
            [self startWorking];
        } else if (error) {
            [UIAlertController alertControllerWithTitle:nil message:[error description] preferredStyle:UIAlertControllerStyleAlert];
        }
    }];
    
}

- (void)startWorking {
    [self performSegueWithIdentifier:NEXT_CONTROLLER_SEGUE_ID sender:self];
}

- (void)vkSdkAccessAuthorizationFinishedWithResult:(VKAuthorizationResult *)result {
    if (result.token) {
        [self startWorking];
    } else if (result.error) {
        [UIAlertController alertControllerWithTitle:nil message:[result.error description] preferredStyle:UIAlertControllerStyleAlert];
    }
}

- (void)vkSdkUserAuthorizationFailed {
    NSLog(@"Try again");
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

- (IBAction)enterVK:(UIButton *)sender {
    [VKSdk authorize:SCOPE];
}

- (void)vkSdkShouldPresentViewController:(UIViewController *)controller {
    [self.navigationController.topViewController presentViewController:controller animated:YES completion:nil];
}

- (void)vkSdkNeedCaptchaEnter:(VKError *)captchaError {
    
}

@end
