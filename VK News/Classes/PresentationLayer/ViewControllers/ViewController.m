//
//  ViewController.m
//  VK News
//
//  Created by Sergey Aleshin on 20/11/2018.
//  Copyright © 2018 MERA. All rights reserved.
//

#import "ViewController.h"
#import "CustomTableViewCell.h"
#import <VKSdk.h>
#import "VKServerManager.h"

static NSInteger numberOfViewedPosts = 20;
static CGSize sizeOfProfilePhoto = {
    36.f,
    36.f
};
static CGSize sizeOfProfilePhotoFromServer = {
    50.f,
    50.f
};
static NSString *nextFromInViewController;

@interface UITableView ()

@end

@interface ViewController () <UISearchBarDelegate, UIScrollViewDelegate>

@property (strong, nonatomic) NSMutableArray *postsArray;
@property (strong, nonatomic) UIBarButtonItem *profileButton;

@property (weak, nonatomic) IBOutlet UILabel *postsCount;
@property (weak, nonatomic) IBOutlet UIView *feedFooter;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *reloadIndicator;

@end

@implementation ViewController

#pragma mark - NavigationBarView

- (void)setSearchBar {
    UIColor *customColor = [[UIColor alloc] initWithRed:231.f/255.f green:232.f/255.f blue:231.f/255.f alpha:1.f];
    UIColor *customColorForSearchField = [[UIColor alloc] initWithRed:217.f/255.f green:219.f/255.f blue:221.f/255.f alpha:1.f];
    UISearchBar *searchBar = [[UISearchBar alloc] init];
    UITextField *searchField = [searchBar valueForKey:@"searchField"];
    
    searchBar.delegate = self;
    searchBar.placeholder = @"Поиск";
    searchBar.barStyle = UIBarStyleDefault;
    searchBar.translucent = NO;
    searchBar.barTintColor = customColor;
    searchBar.backgroundImage = [UIImage new];
    searchField.backgroundColor = customColorForSearchField;
    self.navigationItem.titleView = searchBar;
    self.navigationController.navigationBar.shadowImage = [UIImage new];
}

- (void)setAvatar {
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0.f, 0.f, sizeOfProfilePhoto.width, sizeOfProfilePhoto.height)];
    
    [[VKServerManager sharedManager] getProfileAvatarOnSuccess:^(NSString *urlOfProfilePhoto) {
        NSURL *url = [NSURL URLWithString:urlOfProfilePhoto];
        NSData *data = [NSData dataWithContentsOfURL:url];
        UIImage *image = [UIImage imageWithData:data scale:(sizeOfProfilePhotoFromServer.width / sizeOfProfilePhoto.width)];

        [button setImage:image forState:UIControlStateNormal];
        button.imageView.contentMode = UIViewContentModeCenter;
        button.layer.cornerRadius = button.layer.frame.size.height / 2;
        button.clipsToBounds = TRUE;
        self.profileButton = [[UIBarButtonItem alloc] initWithCustomView:button];

        self.navigationItem.rightBarButtonItem = self.profileButton;
        self.navigationItem.hidesBackButton = YES;
    } onFailure:^(NSError *error, NSInteger statusCode) {
        NSLog(@"error = %@, code = %ld", [error localizedDescription], statusCode);
    }];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    self.navigationItem.rightBarButtonItem = nil;
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    if ([searchBar.text isEqualToString:@""])
        self.navigationItem.rightBarButtonItem = self.profileButton;
}

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
        
    self.postsArray = [NSMutableArray array];
    
    nextFromInViewController = @"0";
    [self setSearchBar];
    [self setAvatar];
    [self getPostsFromServerWithNextFrom:nextFromInViewController];
    [self.navigationController setNavigationBarHidden:NO];
    UIRefreshControl *refresh = [[UIRefreshControl alloc] init];
    [refresh addTarget:self action:@selector(refreshFeedWithNextFrom) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refresh;
}

#pragma mark - API

- (void)refreshFeedWithNextFrom {
    [[VKServerManager sharedManager] getPostsWithCount:numberOfViewedPosts
                                startFromIdentificator:@"0"
                                             onSuccess:^(NSArray *posts, NSString *nextFromParameter) {
                                                 nextFromInViewController = nextFromParameter;
                                                 [self.postsArray removeAllObjects];
                                                 [self.postsArray addObjectsFromArray:posts];
                                                 [self.tableView reloadData];
                                                 [self.refreshControl endRefreshing];
                                             }
                                             onFailure:^(NSError *error, NSInteger statusCode) {
                                                 NSLog(@"error = %@, code = %ld", [error localizedDescription], statusCode);
                                                 [self.refreshControl endRefreshing];
                                             }];
}

- (void)getPostsFromServerWithNextFrom:(NSString *)nextFrom {
    [[VKServerManager sharedManager] getPostsWithCount:numberOfViewedPosts
                                startFromIdentificator:nextFrom
                                              onSuccess:^(NSArray *posts, NSString *nextFromParameter) {
                                                  [self.postsArray addObjectsFromArray:posts];
                                                  nextFromInViewController = nextFromParameter;
                                                  
                                                  NSMutableArray *newPaths = [NSMutableArray array];
                                                  
                                                  for (int i = (int)[self.postsArray count] - (int)[posts count]; i < [self.postsArray count]; i++)
                                                      [newPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
                                                  [self.tableView beginUpdates];
                                                  [self.tableView insertRowsAtIndexPaths:newPaths withRowAnimation:UITableViewRowAnimationTop];
                                                  [self.tableView endUpdates];
                                              }
                                              onFailure:^(NSError * _Nonnull error, NSInteger statusCode) {
                                                  NSLog(@"error = %@, code = %ld", [error localizedDescription], statusCode);
                                              }];
}

#pragma mark - Infinite Scroll

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat actualPosition = scrollView.contentOffset.y;
    CGFloat contentHeight = scrollView.contentSize.height - scrollView.frame.size.height;
    
    if (actualPosition >= contentHeight) {
        [self.postsCount setHidden:YES];
        [self.reloadIndicator setHidden:NO];
        [self.reloadIndicator startAnimating];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(nonnull UITableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if (indexPath.row == [self.postsArray count] - 1) {
        [self getPostsFromServerWithNextFrom:nextFromInViewController];
        [self.reloadIndicator stopAnimating];
        [self.reloadIndicator setHidden:YES];
        [self.postsCount setHidden:NO];
    }
}

#pragma mark - TableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (([self.postsArray count] >= 11 && [self.postsArray count] <= 14) || ([self.postsArray count] % 10 >= 5 && [self.postsArray count] % 10 <= 9) || ([self.postsArray count] % 10 == 0))
        self.postsCount.text = [NSString stringWithFormat:@"%lu записей", [self.postsArray count]];
    else if ([self.postsArray count] % 10 >= 2 && [self.postsArray count] % 10 <= 4)
        self.postsCount.text = [NSString stringWithFormat:@"%lu записи", [self.postsArray count]];
    else if ([self.postsArray count] % 10 == 1)
        self.postsCount.text = [NSString stringWithFormat:@"%lu запись", [self.postsArray count]];
    
    return [self.postsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CustomTableViewCell *specialCell = [tableView dequeueReusableCellWithIdentifier:@"PostCell"];
    
    [specialCell configureForPost:self.postsArray[indexPath.row]];
    
    return specialCell;
}

@end
