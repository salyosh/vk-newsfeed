//
//  CustomTableViewCell.h
//  VK News
//
//  Created by Sergey Aleshin on 26/11/2018.
//  Copyright © 2018 MERA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Post.h"

@interface CustomTableViewCell : UITableViewCell

- (void)configureForPost:(Post *)post;

@end
