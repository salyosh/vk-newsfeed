//
//  CustomTableViewCell.m
//  VK News
//
//  Created by Sergey Aleshin on 26/11/2018.
//  Copyright © 2018 MERA. All rights reserved.
//

#import "CustomTableViewCell.h"

static NSString *emptyString = @"";
static CGFloat zeroValueForConstraint = 0.f;

@interface CustomTableViewCell ()

@property (weak, nonatomic) IBOutlet UIView *componentsOfNews;
@property (weak, nonatomic) IBOutlet UIImageView *avatarOfSource;
@property (weak, nonatomic) IBOutlet UILabel *nameOfSource;
@property (weak, nonatomic) IBOutlet UILabel *dateOfPost;
@property (weak, nonatomic) IBOutlet UITextView *textOfPost;
@property (weak, nonatomic) IBOutlet UIImageView *imageInPost;
@property (weak, nonatomic) IBOutlet UIButton *likesWithNumberButton;
@property (weak, nonatomic) IBOutlet UIButton *commentsWithNumberButton;
@property (weak, nonatomic) IBOutlet UIButton *sharesWithNumberButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *postTextHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *postImageHeight;

@end

@implementation CustomTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self configureUI];
}

- (void)configureUI {
    UIColor *customColor = [[UIColor alloc] initWithRed:231.f/255.f green:232.f/255.f blue:231.f/255.f alpha:1.f];
    [self setBackgroundColor:customColor];
    self.avatarOfSource.layer.cornerRadius = self.avatarOfSource.frame.size.width / 2;
    self.avatarOfSource.clipsToBounds = TRUE;
    self.componentsOfNews.layer.cornerRadius = 10;
}

- (void)configureForPost:(Post *)post {
    self.nameOfSource.text = post.nameOfSource;
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        NSData *dataForAvatar = [NSData dataWithContentsOfURL:post.urlOfSourcesAvatar];
        UIImage *imageOfAvatar = [UIImage imageWithData:dataForAvatar];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.avatarOfSource setImage:imageOfAvatar];
        });
    });
    
    NSDateFormatter *formatterDay = [[NSDateFormatter alloc] init];
    formatterDay.locale = [NSLocale localeWithLocaleIdentifier:@"ru"];
    NSDateFormatter *formatterTime = [[NSDateFormatter alloc] init];
    [formatterDay setDateFormat:@"d MMMM"];
    [formatterTime setDateFormat:@"HH:mm"];
    self.dateOfPost.text = [NSString stringWithFormat:@"%@ в %@", [formatterDay stringFromDate:post.dateOfPost], [formatterTime stringFromDate:post.dateOfPost]];
    
    self.textOfPost.text = post.textOfPost;
    if ([post.textOfPost isEqualToString:emptyString]) {
        [self.postTextHeight setActive:YES];
    } else {
        [self.postTextHeight setActive:NO];
    }
    
    NSURL *emptyURL = [NSURL URLWithString:emptyString];
    if ([post.urlOfPostsPhoto isEqual:emptyURL]) {
        self.postImageHeight.constant = zeroValueForConstraint;
    } else {
        self.postImageHeight.constant = self.componentsOfNews.frame.size.width * post.heightPhoto / post.widthPhoto;
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            NSData *dataForPhoto = [NSData dataWithContentsOfURL:post.urlOfPostsPhoto];
            UIImage *imageOfPhoto = [UIImage imageWithData:dataForPhoto];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.imageInPost setImage:imageOfPhoto];
            });
        });
    }
    
    if (post.numberOfLikes)
        [self.likesWithNumberButton setTitle:[NSString stringWithFormat:@" %ld", (long)post.numberOfLikes] forState:UIControlStateNormal];
    else
        [self.likesWithNumberButton setTitle:@" " forState:UIControlStateNormal];
    if (post.numberOfComments)
        [self.commentsWithNumberButton setTitle:[NSString stringWithFormat:@" %ld", (long)post.numberOfComments] forState:UIControlStateNormal];
    else
        [self.commentsWithNumberButton setTitle:@" " forState:UIControlStateNormal];
    if (post.numberOfShares)
        [self.sharesWithNumberButton setTitle:[NSString stringWithFormat:@" %ld", (long)post.numberOfShares] forState:UIControlStateNormal];
    else
        [self.sharesWithNumberButton setTitle:@" " forState:UIControlStateNormal];
}

@end
